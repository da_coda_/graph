package Data;

import Logic.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FileWriter {

    private Controller c;
    private java.io.FileWriter fw;

    public FileWriter(Controller c){
        this.c = c;
    }

    public void writeFile(){
        try {
            fw = new java.io.FileWriter("graph.txt");
            ArrayList<Edge> edges = c.getEdges();
            HashMap<String, Point> points = c.getPoints();
            for (Point p: points.values()) {
                fw.append(p.toString());
                fw.append("\n");
            }
            fw.append("<Kanten>");
            fw.append("\n");
            for(Edge e: edges){
                fw.append(e.toString());
                fw.append("\n");
            }
        }catch(IOException io){
            System.out.println("Datei konnte nicht geschrieben werden");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(fw != null){
                try {
                fw.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

}
