package Data;

public class Point {

    private String text;
    private int x;


    private int y;

    public Point(String text, int x, int y){
        this.text = text;
        this.x = x;
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getText(){
        return text;
    }

    public int[] getPoint(){
        return new int[]{x, y};
    }

    @Override
    public String toString() {
        return text;

    }
}
