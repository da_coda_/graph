package Data;

import Logic.Controller;

import java.util.ArrayList;
import java.util.function.Predicate;

public class Edges extends ArrayList<Edge>{

    private Controller c;

    public Edges(Controller c){
        this.c = c;
    }

    public int addEdge(Point from, Point to, int value){
        for(Edge e: this){
            if(e.getFrom() == from && e.getTo() == to){
                return -1;
            }
        }
        add(new Edge(from, to, value));
        return 0;
    }

    public void removeEdgeForPoint(Point p){
        Predicate<Edge> removeEdge = (Edge e) -> e.getTo().getText().equals(p.getText()) || e.getFrom().getText().equals(p.getText());
        this.removeIf(removeEdge);
    }

    public void removeEdgeForPoints(String p1, String p2){
            Predicate<Edge> removeEdge = (Edge e) -> e.getTo().getText().equals(p2) && e.getFrom().getText().equals(p1);
            this.removeIf(removeEdge);
    }

    public Edges getEdges(){
        return this;
    }



}
