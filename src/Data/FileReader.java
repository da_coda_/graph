package Data;

import Logic.Controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {

    private Controller c;
    private java.io.FileReader fr;
    private BufferedReader br;

    public FileReader(Controller c){
        this.c = c;
    }

    public void readFile(){
        try {
            fr = new java.io.FileReader("graph.txt");
            br = new BufferedReader(fr);
            String line = br.readLine();
            while(line != null && !line.equals("<Kanten>")){
                c.addNewPoint(line);
                line = br.readLine();
            }
            line = br.readLine();
            while(line != null){
                String[] in = line.split("<->");
                String p1 = in[0].trim();
                String[] rest = in[1].split(":");
                String p2 = rest[0].trim();
                int value = Integer.parseInt(rest[1].trim());
                c.addNewEdge(c.getPoints().get(p1), c.getPoints().get(p2), value);
                line = br.readLine();
            }
        }catch(FileNotFoundException e){
            System.out.println("Datei nicht gefunden");
        } catch (IOException e){
            e.printStackTrace();
        }
        finally {
            try{
                if(br != null) {
                    br.close();
                    fr.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
