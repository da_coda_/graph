package Data;

public class Edge {
    private Point from;
    private Point to;
    private int value;

    public Edge(Point from, Point to, int value){
        this.from = from;
        this.to = to;
        this.value = value;
    }

    public Point getFrom(){
        return from;
    }

    public Point getTo() {
        return to;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return from.getText() + " <-> " + to.getText() + ": " + value;
    }
}

