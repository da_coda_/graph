package Data;

import Logic.Controller;

import java.util.HashMap;

public class Points extends HashMap<String, Point>{
    private boolean[][] points;

    private Controller c;

    public Points(Controller c){
        this.points = new boolean[6][3];
        this.c = c;
    }

    public int addNewPoint(String text){
        if(containsKey(text)){
            return -1;
        }else {
            int amount = size() + 1;
            int radius = 300;
            double theta = ((Math.PI*2)/amount);
            calcPointXY(300, theta);
            double angle = theta * amount;
            double x = (radius * Math.cos(angle));
            double y = (radius * Math.sin(angle));
            put(text, new Point(text, (int) Math.round(x) + 525, (int) Math.round(y) + 300));
            return 0;
        }
    }

    private void calcPointXY(int radius, double theta){
        int i = 1;
        for(Point p: this.values()){
            double angle = theta * i;
            double x = (radius * Math.cos(angle));
            p.setX((int) Math.round(x) + 525);
            double y = (radius * Math.sin(angle));
            p.setY((int) Math.round(y) + 300);
            i++;
        }
    }

    public Points getPoints(){
        return this;
    }

    public void removePoint(String text){
        c.removeEdgeForPoint(this.get(text));
        remove(text);
        int amount = size();
        int radius = 300;
        double theta = ((Math.PI*2)/amount);
        calcPointXY(radius, theta);
    }

    @Override
    public String toString(){
        String points = "";
        points += "Länge: " + size();
        return points;
    }

}
