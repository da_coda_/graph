package Logic;

import Data.*;
import GUI.Window;

import java.util.ArrayList;
import java.util.HashMap;

public class Controller {
    private Points points;
    private Window window;
    private FileReader fr;
    private FileWriter fw;
    private Edges edges;
    private boolean debug;

    public Controller(boolean debug){

        this.debug = debug;

        // Model init
        edges  = new Edges(this);
        points = new Points(this);
        window = new Window(this);

        // Helper init
        fr = new FileReader(this);
        fw = new FileWriter(this);
    }

    public int addNewPoint(String text){
        if(text.isEmpty() || text.trim().length() <= 0) return -2;
        if(text.trim().charAt(0) == '<') return -3;
        return points.addNewPoint(text);
    }

    public void removeEdgeForPoint(Point p){
        edges.removeEdgeForPoint(p);
    }

    public void readFile(){
        fr.readFile();
    }

    public void writeFile(){
        fw.writeFile();
    }

    public int addNewEdge(Point from, Point to, int value){
        return edges.addEdge(from, to, value);
    }

    public HashMap<String, Point> getPoints(){
        return points.getPoints();
    }

    public ArrayList<Edge> getEdges(){
        return edges.getEdges();
    }

    public void removePoint(String text){
        points.removePoint(text);
    }

    public void removeEdgeForPoints(String p1, String p2){
        edges.removeEdgeForPoints(p1, p2);
    }

    public boolean getDebug(){
        return debug;
    }

    public void debug(){
        String pointsString = "";
        for(String text: points.getPoints().keySet()){
            pointsString += text + "\n";
        }
        System.out.println("Punkte: \n" + pointsString);
        System.out.println("Points: \n" + points.toString());
    }
}
