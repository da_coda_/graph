package Logic;

public class Main {
    public static void main(String[] args){
        boolean isDebug = false;
        if(args.length > 0){
            if(args[0].compareTo("debug") == 0)
                isDebug = true;
        }
        new Controller(isDebug);
    }
}
