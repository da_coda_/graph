package GUI;

import Data.Edge;
import Data.Point;
import Logic.Controller;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Window extends JFrame {

    private Controller c;
    private JPanel pnl_backgound;
    private JButton btn_addPoint;
    private JButton btn_addEdge;
    private JButton btn_removePoint;
    private JButton btn_removeEdge;
    private JTextField txt_pointName;
    private JComboBox<String> jcb_addEdgePoint1;
    private JComboBox<String> jcb_addEdgePoint2;
    private JComboBox<String> jcb_removePoint;
    private JComboBox<String> jcb_removeEdge;
    private JButton btn_debug;
    private JLabel lbl_status;
    private JTextField txt_edgeValue;
    private Graph graph;
    private JButton write;
    private JButton read;

    public Window(Controller c){
        this.c = c;
        initElements();
    }

    private void initElements(){
        initButtons();
        initStatus();
        initTextFields();
        initComboBoxes();
        initBackground();
        initGraph();
        initWindow();
    }

    private void initButtons(){
        btn_addPoint = new JButton();
        btn_addEdge = new JButton();
        btn_removeEdge = new JButton();
        btn_removePoint = new JButton();
        write = new JButton();
        read = new JButton();


        btn_addPoint.setBounds(200, 50, 130, 50);
        btn_removePoint.setBounds(200, 120, 130, 50);
        btn_addEdge.setBounds(700, 50, 130, 50);
        btn_removeEdge.setBounds(700, 120, 130, 50);
        write.setBounds(930, 50, 130, 50);
        read.setBounds(930, 120, 130, 50);


        btn_addPoint.setFont(new Font("Serif", Font.PLAIN, 10));
        btn_addPoint.setText("Knoten hinzufügen");
        btn_addPoint.addActionListener(ae -> {
            switch(c.addNewPoint(txt_pointName.getText())){
                case -1: lbl_status.setText("Knoten mit diesem Text schon vorhanden");
                         break;
                case -2: lbl_status.setText("Kein Name für den Knoten angegeben");
                         break;
                case -3: lbl_status.setText("Ungültiges Zeichen am Anfang des Namen");
                         break;
                case 0:  lbl_status.setText("Knoten erfolgreich hinzugefügt");
                         break;
                default: lbl_status.setText("Ein unerwarteter Fehler ist aufgetreten");
            }
            txt_pointName.setText("");
            graph.repaint();
            fillJcbRemovePoint();
            fillJcbAddEdgePoint1();
            fillJcbAddEdgePoint2();
            fillJcbRemoveEdge();
        });
        btn_removePoint.setFont(new Font("Serif", Font.PLAIN, 10));
        btn_removePoint.setText("Knoten entfernen");
        btn_removePoint.addActionListener(ae -> {
            if(jcb_removePoint.getItemCount() == 0){
                lbl_status.setText("Es gibt keine Punkte die entfernt werden können");
            }
            else{
                c.removePoint((String) jcb_removePoint.getSelectedItem());
                graph.repaint();
                fillJcbRemovePoint();
                fillJcbAddEdgePoint1();
                fillJcbAddEdgePoint2();
                fillJcbRemoveEdge();
            }
        });
        btn_addEdge.setFont(new Font("Serif", Font.PLAIN, 10));
        btn_addEdge.setText("Kante hinzufügen");
        btn_addEdge.addActionListener(ae -> {
            Point from = c.getPoints().get(jcb_addEdgePoint1.getSelectedItem());
            Point to = c.getPoints().get(jcb_addEdgePoint2.getSelectedItem());
            if(from == to){
                lbl_status.setText("Bitte wählen sie unterschiedliche Punkte aus");
            }
            else{
                int val;
                try {
                   val = Integer.parseInt(txt_edgeValue.getText());
                    switch (c.addNewEdge(from, to, val)){
                        case -1: lbl_status.setText("Kante zwischen den beiden Punkten schon vorhanden");
                        break;
                        case 0: lbl_status.setText("Kannte erfolgreich hinzugefügt");
                        break;
                        default: lbl_status.setText("Unerwarteter Fehler");
                        break;
                    }
                    graph.repaint();
                } catch (NumberFormatException ex) {
                    lbl_status.setText("Bitte geben sie eine Zahl als Wert an");
                }
                fillJcbRemoveEdge();
            }

        });
        btn_removeEdge.setFont(new Font("Serif", Font.PLAIN, 10));
        btn_removeEdge.setText("Kante entfernen");
        btn_removeEdge.addActionListener(ae ->{
            String remove = jcb_removeEdge.getSelectedItem().toString();
            String[] parts = remove.split("<->");
            c.removeEdgeForPoints(parts[0].trim(), parts[1].split(":")[0].trim());
            graph.repaint();
            fillJcbRemoveEdge();
        });
        btn_debug = new JButton();
        btn_debug.setText("Debug");
        btn_debug.setBounds(1500, 10, 80, 20);
        btn_debug.setFont(new Font("Serif", Font.PLAIN, 10));
        btn_debug.addActionListener(ae -> c.debug());

        write.setText("Datei schreiben");
        write.setFont(new Font("Serif", Font.PLAIN, 10));
        write.addActionListener(ae ->
            c.writeFile()
        );

        read.setText("Datei lesen");
        read.setFont(new Font("Serif", Font.PLAIN, 10));
        read.addActionListener(ae -> {
            c.readFile();
            graph.repaint();
            fillJcbRemoveEdge();
            fillJcbAddEdgePoint2();
            fillJcbAddEdgePoint1();
            fillJcbRemovePoint();
        });

    }

    private void initStatus(){
        lbl_status = new JLabel();
        lbl_status.setBounds(10, 10, 1030, 30);
        lbl_status.setFont(new Font("Serif", Font.PLAIN, 10));
    }

    private void initComboBoxes(){
        jcb_removePoint = new JComboBox<>();
        jcb_addEdgePoint1 = new JComboBox<>();
        jcb_addEdgePoint2 = new JComboBox<>();
        jcb_removeEdge = new JComboBox<>();

        jcb_removePoint.setBounds(50, 130, 130, 30);
        jcb_addEdgePoint2.setBounds(400, 80, 130, 30);
        jcb_addEdgePoint1.setBounds(400, 50, 130, 30);
        jcb_removeEdge.setBounds(400, 130, 280, 30);

    }

    private void initTextFields(){
        txt_pointName = new JTextField();
        txt_edgeValue = new JTextField();
        txt_pointName.setBounds(50, 60, 130, 30);
        txt_pointName.setToolTipText("Name des Punktes");
        txt_edgeValue.setBounds(550, 50, 130, 30);
        txt_edgeValue.setToolTipText("Wert der Kante");
    }

    private void initWindow(){
        setPreferredSize(new Dimension(1700, 900));
        add(pnl_backgound);
        add(graph);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        pack();
    }

    private void initBackground(){
        pnl_backgound = new JPanel();
        pnl_backgound.setLayout(null);
        pnl_backgound.setSize(1700, 200);
        pnl_backgound.add(btn_addEdge);
        pnl_backgound.add(btn_addPoint);
        pnl_backgound.add(btn_removeEdge);
        pnl_backgound.add(btn_removePoint);
        pnl_backgound.add(jcb_removePoint);
        pnl_backgound.add(jcb_addEdgePoint2);
        pnl_backgound.add(jcb_addEdgePoint1);
        pnl_backgound.add(txt_pointName);
        pnl_backgound.add(jcb_removeEdge);
        pnl_backgound.add(lbl_status);
        pnl_backgound.add(txt_edgeValue);
        pnl_backgound.add(write);
        pnl_backgound.add(read);
        if(c.getDebug()){
            pnl_backgound.add(btn_debug);
        }
    }

    private void initGraph(){
        graph = new Graph(c);
        graph.setSize(1700, 700);
        graph.setLocation(0, 200);
    }

    private void fillJcbRemovePoint(){
        jcb_removePoint.removeAllItems();
        HashMap<String, Point> points = c.getPoints();
        for(Map.Entry<String, Point> p: points.entrySet()){
            jcb_removePoint.addItem(p.getKey());
        }
    }

    private void fillJcbAddEdgePoint1(){
        jcb_addEdgePoint1.removeAllItems();
        HashMap<String, Point> points = c.getPoints();
        for(Map.Entry<String, Point> p: points.entrySet()){
            jcb_addEdgePoint1.addItem(p.getKey());
        }
    }

    private void fillJcbAddEdgePoint2(){
        jcb_addEdgePoint2.removeAllItems();
        HashMap<String, Point> points = c.getPoints();
        for(Map.Entry<String, Point> p: points.entrySet()){
            jcb_addEdgePoint2.addItem(p.getKey());
        }
    }

    private void fillJcbRemoveEdge(){
        jcb_removeEdge.removeAllItems();
        ArrayList<Edge> edges = c.getEdges();
        for(Edge e: edges){
            jcb_removeEdge.addItem(e.toString());
        }
    }


}
