package GUI;

import Data.Edge;
import Data.Point;
import Logic.Controller;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Graph extends JPanel {

    private Controller c;

    public Graph(Controller c){
        this.c = c;
    }


    public void paintComponent(Graphics g) {
        HashMap<String, Point> points = c.getPoints();
        ArrayList<Edge> edges = c.getEdges();
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        for(Edge e: edges){
            g2.drawLine(e.getFrom().getPoint()[0], e.getFrom().getPoint()[1]+25, e.getTo().getPoint()[0], e.getTo().getPoint()[1]+25);
            int x = (e.getTo().getPoint()[0] - e.getFrom().getPoint()[0]) / 2;
            int y = (e.getTo().getPoint()[1] - e.getFrom().getPoint()[1]) / 2;
            g2.drawString(e.getValue() + "", e.getFrom().getPoint()[0] + x, e.getFrom().getPoint()[1] + y);
        }
        for(Point p: points.values()){
            g2.setColor(Color.YELLOW);
            g2.fillOval(p.getPoint()[0] - 25, p.getPoint()[1], 50, 50);
            g2.setColor(Color.BLACK);
            g2.drawOval(p.getPoint()[0] - 25, p.getPoint()[1], 50, 50);
            int stringLen = (int) g2.getFontMetrics().getStringBounds(p.getText(), g2).getWidth();
            int start = 50/2 - stringLen/2;
            g2.drawString(p.getText(), p.getPoint()[0] + start - 25, p.getPoint()[1] + 30);
        }

    }
}
